#!/usr/bin/env python3
"""Looping with 'For'."""


def main():
    """Script entry point."""
    print("My name is")
    for i in range(5):
        print(f"Jimmy Five Times ({i:02d})")


if __name__ == '__main__':
    main()
