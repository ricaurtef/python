#!/usr/bin/env python3
"""Argparse:

Conflicting Options.
- It allows for us to specify options that conflict with each other.
"""
import argparse


def args():
    """Handles command line options."""
    parser = argparse.ArgumentParser(
        description='Calculate X to the power of Y.'
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-v', '--verbose', action='store_true')
    group.add_argument('-q', '--quiet', action='store_true')
    parser.add_argument('x', type=int, help='the base')
    parser.add_argument('y', type=int, help='the exponent')
    return parser.parse_args()


def main():
    """Script entry point."""
    arguments = args()
    answer = pow(arguments.x, arguments.y)
    if arguments.quiet:
        print(answer)
    elif arguments.verbose:
        print(f'{arguments.x} to the power {arguments.y} equals {answer}.')
    else:
        print(f'{arguments.x}^{arguments.y} == {answer}.')


if __name__ == '__main__':
    main()
