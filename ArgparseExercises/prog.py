#!/usr/bin/env python3
"""Argparse:

The basics.
- Introducing Positional Arguments.
"""
import argparse


def main():
    """Script entry point."""
    parser = argparse.ArgumentParser()
    # Position argument
    parser.add_argument('square', type=int,
                        help='display a square of a given number')
    args = parser.parse_args()
    print(pow(args.square, 2))


if __name__ == '__main__':
    main()
