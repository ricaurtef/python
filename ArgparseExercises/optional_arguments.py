#!/usr/bin/env python3
"""Argparse:

The basics.
- Introducing Optional Arguments.
"""
import argparse


def main():
    """Script entry point."""
    parser = argparse.ArgumentParser()
    # Short and long options.
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity')
    args = parser.parse_args()
    if args.verbose:
        print('verbosity turned on.')


if __name__ == '__main__':
    main()
