#!/usr/bin/env python3
"""Exercise.

Simple temperature convertor.
"""
import argparse


class Temperature:
    """Simple attempt to model a temperature class."""
    def __init__(self, value):
        """Initialize Temperature class."""
        self.value = value

    def convert_to_fahrenheit(self):
        """Converts a given value in Celsius to Fahrenheit."""
        return self.value * (9/5) + 32

    def convert_to_celsius(self):
        """Converts a given value in Fahrenheit to Celsius."""
        return (self.value - 32) * (5/9)


def args():
    """Handles command line options."""
    parser = argparse.ArgumentParser(description='temperature convertor')
    group = parser.add_mutually_exclusive_group(required=True)

    # Temperature value
    parser.add_argument('value', type=float,
                        help='value to be converted')

    # Conversion options
    group.add_argument('-f', '--fahrenheit', action='store_true',
                       help='convert to fahrenheit')
    group.add_argument('-c', '--celsius', action='store_true',
                       help='convert to celsius')
    return parser.parse_args()


def main():
    """Script entry point."""
    temperature = Temperature(args().value)

    if args().fahrenheit:
        converted_value = temperature.convert_to_fahrenheit()
        degrees = ('C', 'F')
    elif args().celsius:
        converted_value = temperature.convert_to_celsius()
        degrees = ('F', 'C')

    print(f'{temperature.value} degrees {degrees[0]} = '
          f'{converted_value:.1f} degrees {degrees[1]}')


if __name__ == '__main__':
    main()
