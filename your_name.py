#!/usr/bin/env python3
'''Flow control statements example.

An annoying while loop.
'''


def main():
    '''"your_name.py" entry point.'''

    while True:
        name = input('Please type your name: ').lower()
        if name == 'your name':
            break
    print('Thank you!')


if __name__ == '__main__':
    main()
