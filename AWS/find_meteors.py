#!/usr/bin/env python3
"""Find Meteors:

A demo project that uses Python and NASA data to find meteor landing
sites.
"""
import math
import requests
from tabulate import tabulate


def calc_dist(lat1, lon1, lat2, lon2):
    lat1 = math.radians(lat1)
    lon1 = math.radians(lon1)
    lat2 = math.radians(lat2)
    lon2 = math.radians(lon2)

    h = math.sin((lat2 - lat1) / 2) ** 2 + math.cos(lat1) * math.cos(lat2) \
        * math.sin((lon2 - lon1) / 2) ** 2
    return 6372.8 * 2 * math.asin(math.sqrt(h))


def get_dist(meteor):
    return meteor.get('distance', math.inf)


def main():
    """Entry point."""
    # 'Martin G. Merou' coordinates.
    my_loc = {'reclat': -34.740476, 'reclong': -58.603829}

    meteor_resp = requests.get('https://data.nasa.gov/resource/y77d-th95.json')
    meteor_data = meteor_resp.json()

    for meteor in meteor_data:
        if not ('reclat' in meteor and 'reclong' in meteor):
            continue
        meteor['distance'] = calc_dist(float(meteor['reclat']),
                                       float(meteor['reclong']),
                                       my_loc['reclat'],
                                       my_loc['reclong'])
    meteor_data.sort(key=get_dist)
    print(tabulate(meteor_data[0:10], headers='keys'))


if __name__ == '__main__':
    main()
