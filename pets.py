#!/usr/bin/env python3
"""Functions: Positional Arguments."""


def describe_pet(pet_name, animal_type='dog'):
    """Display information about a pet."""
    print(f'\nI have a {animal_type}.')
    print(f"My {animal_type}'s name is {pet_name.capitalize()}.")


def main():
    """Script entry point."""
    describe_pet('harry', 'hamster')
    describe_pet('chocolate')


if __name__ == '__main__':
    main()
