#!/usr/bin/env python3
"""Module Import."""
import random


def main():
    """Script entry point."""
    for _ in range(5):
        print(random.randint(1, 10))


if __name__ == '__main__':
    main()
