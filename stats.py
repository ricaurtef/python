#!/usr/bin/env python3
"""Challenge:

Lists of Lists.
"""


# Sample list
UNIVERSITIES = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]


def median(values):
    """Returns the median of a given list of values."""
    values.sort()
    length = len(values)
    if not length % 2:
        return values[(length - 1) / 2]
    return values[int(length / 2)]


def mean(values):
    """Returns the mean of a given list of values."""
    return sum(values) / len(values)


def enrollment_stats():
    """This function should return two lists:

    First: containing all of the student enrollment values.
    Second: containing all of the tuition fees.
    """
    students = [value[1] for value in UNIVERSITIES]
    tuitions = [value[2] for value in UNIVERSITIES]
    return (students, tuitions)


def main():
    """Entry point."""
    t_format = '12,.2f'
    s_format = '12,.0f'
    students, tuitions = enrollment_stats()

    # Output:
    print('*' * 30)
    print(f'Total Students:\t{sum(students):{s_format}}')
    print(f'Total Tuition:\t{sum(tuitions):{t_format}}\n')
    print(f'Student Mean:\t{mean(students):{s_format}}')
    print(f'Student Median:\t{median(students):{s_format}}\n')
    print(f'Tuition Mean:\t{mean(tuitions):{t_format}}')
    print(f'Tuition Median:\t{median(tuitions):{t_format}}')
    print('*' * 30)


if __name__ == '__main__':
    main()
