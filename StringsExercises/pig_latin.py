#!/usr/bin/env python3
"""
Strings:

Pig Latin is a silly made-up language that alters English words. If a
word begins with a vowel, the word yay is added to the end of it. If a
word begins with a consonant or consonant cluster (like ch or gr), that
consonant or cluster is moved to the end of the word followed by ay.
"""
import re


def pig_consonant(word):
    """Pass."""
    # Case insensitive match
    consonants = re.compile(r'([^aeiou]+)([aeiouy]\w*)', re.I)
    mo = consonants.match(word)
    suffix = 'AY' if word.isupper() else 'ay'
    altered_word = f'{mo.group(2)}{mo.group(1)}{suffix}'
    return altered_word


def pig_vowel(word):
    """Pass."""
    # Case insensitive match
    vowels = re.compile(r'([aeiou]\w*)', re.I)
    mo = vowels.match(word)
    suffix = 'YAY' if word.isupper() else 'yay'
    altered_word = f'{mo.group()}{suffix}'
    return altered_word


def main():
    """Script entry point."""
    # TODO: Draft input section.
    TEXT = 'My name is AL SWEIGART and I am 4,000 years old.'
    pig_latin = []
    for word in TEXT.split():
        if re.search('[a-zA-Z]+', word):
            if re.match('[aeiouAEIOU]', word):
                pig_latin.append(pig_vowel(word))
            else:
                pig_latin.append(pig_consonant(word))
        else:
            pig_latin.append(word)
    pig_latin[0] = pig_latin[0].capitalize()
    print(' '.join(pig_latin))


if __name__ == '__main__':
    main()
