#!/usr/bin/env python3
"""Strings exercises: Table Printer.

Write a function named printTable() that takes a list of lists of
strings and displays it in a well-organized table with each column
right-justified.
"""
from tabulate import tabulate


class Table:
    """A simple class to handle tabular data."""

    def __init__(self, table):
        """Initialize the table class."""
        self.table = table

    def print_table(self):
        """Transverse a pretty print the table right-justified."""
        transverse_table = [*zip(*self.table)]
        print(tabulate(transverse_table, tablefmt='psql', stralign='right'))


def main():
    """Entry point."""
    table_data = [
        ['apples', 'oranges', 'cherries', 'banana'],
        ['Alice', 'Bob', 'Carol', 'David'],
        ['dogs', 'cats', 'moose', 'goose']
    ]

    table = Table(table_data)
    table.print_table()     # Pretty printing.


if __name__ == '__main__':
    main()
