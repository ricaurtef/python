#!/usr/bin/env python3
'''This program says hello and asks for my name.'''
from random import choice


def error_message():
    """Returns a random message in case of an error."""
    messages = [
        'Why do you not want to tell me your age?',
        'That old, ah?!',
        'Is it that hard to enter a number?',
        'I think you chew the water, old guy!'
    ]
    return choice(messages)


def main():
    '''"hello.py" entry point.'''
    print("Hello!")
    my_name = input('What is your name? ').title()
    print(f'It is good to meet you, {my_name}!')
    print(f'The length of your name is: {len(my_name)}')
    for _ in range(3):
        try:
            my_age = int(input('What is your age? '))    # Ask for their age.
        except ValueError:
            continue
        break
    else:
        print(error_message())
        exit(1)

    # If everything goes fine, you will be...
    print(f'You will be {my_age + 1} in a year.')


if __name__ == '__main__':
    main()
