#!/usr/bin/env python3
'''Flow control statements example.'''


def main():
    '''"vampire.py" entry point.'''
    name = input('Enter your name: ').lower()
    if name == 'alice':
        print('Hi, Alice.')
    else:
        age = int(input('Enter your age: '))
        if age < 12:
            print('You are not Alice, kiddo.')
        elif age > 2000:
            print('Unlike you, Alice is not an undead, immortal vampire.')
        elif age > 70:
            print('You are not Alice, grannie.')
        else:
            print(f'Hello, {name.title()}.')


if __name__ == '__main__':
    main()
