#!/usr/bin/env python3
"""Strong Password Detection.

A strong password is defined as one that is at least eight characters
long, contains both uppercase and lowercase characters, and has at
least one digit.
"""
import re
import argparse


def is_strong(passwd):
    """Validates if a given password is strong enough."""
    passwd_re = re.compile(r'[a-zA-Z0-9]{8,}')
    return passwd_re.match(passwd)


def main():
    """strong_password.py entry point."""
    parser = argparse.ArgumentParser(
        description='passsword validator'
    )
    parser.add_argument('password', help='password to be validated')
    args = parser.parse_args()

    if is_strong(args.password):
        print(f'{args.password!r} is a strong password.')
    else:
        print(f'Sorry! {args.password!r} is not strong enough.')


if __name__ == '__main__':
    main()
