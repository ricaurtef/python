#!/usr/bin/env python3
"""REGEX

Finds phone numbers and email addresses on the clipboard.
"""
import re
import pyperclip


def get_phone_numbers(text):
    """Retrieve all the phones numbers in a given text."""
    phone_re = re.compile(r'''(
        (?P<code>\d{3}|\(\d{3}\))?                  # area code
        (?P<sep>\s|-|\.)?                           # separator
        (?P<first>\d{3})                            # first 3 digits
        (?P=sep)                                    # separator
        (?P<last>\d{4})                             # last 4 digitts
        (?P<ext>\s*(ext|x|ext.)\s*(\d{2,5}))?       # extension
    )''', re.X)
    return phone_re.finditer(text)


def get_emails(text):
    """Retrieve all the email addresses in a given text."""
    email_re = re.compile(r'''(
        [a-zA-Z0-9._%+-]+       # username
        @                       # @ symbol
        [a-zA-Z0-9.-]+          # domain name
        (?:\.[a-zA-Z]{2,4})     # top-level domain
    )''', re.X)
    return email_re.finditer(text)


def main():
    """Entry point."""
    text = str(pyperclip.paste())
    matches = []
    for mo in get_phone_numbers(text):
        phone_num = '-'.join([
            mo.group('code'),
            mo.group('first'),
            mo.group('last'),
        ])
        if mo.group('ext'):
            phone_num += ' x' + mo.group('ext')
        matches.append(phone_num)
    for mo in get_emails(text):
        matches.append(mo.group())

    # Copy results to the clipboard and print them to stout.
    if len(matches):
        phones_emails_found = '\n'.join(matches)
        pyperclip.copy(phones_emails_found)
        print(phones_emails_found)
    else:
        print('No phone numbers or email addresses found.')


if __name__ == '__main__':
    main()
