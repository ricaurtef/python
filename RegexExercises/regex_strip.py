#!/usr/bin/env python3
"""Regex practice project: Regex Version of the strip() Method.

Write a function that takes a string and does the same thing as the strip()
string method. If no other arguments are passed other than the string to
strip, then whitespace characters will be removed from the beginning and end
of the string. Otherwise, the characters specified in the second argu- ment to
the function will be removed from the string.
"""
import re


class Text:
    """A simple class to manipulate streams of text."""

    def __init__(self, text):
        """Initialize the text class."""
        self.text = text

    def strip(self, char=r'\s'):
        """
        It should does what the strip() string method does through a regex.
        """
        pattern = r'''
            ^([{char}]*)    # Char to strip.
            (?P<text>.*?)   # Text to return.
            \1$             # Char to strip.
        '''.format(char=char)
        strip_regex = re.compile(pattern, re.X)
        match = strip_regex.search(self.text)
        if match:
            return match.group('text')
        return self.text


def main():
    """Entry point."""
    test_text = Text('spamHello, World!spam')
    print(test_text.strip('mpas'))


if __name__ == '__main__':
    main()
