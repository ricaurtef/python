#!/usr/bin/env python3
"""Four Pillars of OOP: Inheritance.

Multiple inheritance example.
"""


class MUsicalInstrument:
    number_major_keys = 12


class StringInstrument(MUsicalInstrument):
    type_of_wood = 'Tonewood'


class Guitar(StringInstrument):
    def __init__(self):
        self.number_of_strings = 6

    def display_details(self):
        print(f'This guitar consists of {self.number_of_strings} strings. '
              f'It is made of {self.type_of_wood} and it can play '
              f'{self.number_major_keys} keys.')


def main():
    """Enty point."""
    guitar = Guitar()
    guitar.display_details()


if __name__ == '__main__':
    main()
