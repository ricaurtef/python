#!/usr/bin/env python3
"""Practice project.

Fanstasy Game Inventory.
"""


class Inventory:
    """Manage the inventory of a player."""
    def __init__(self, inventory):
        self.inventory = inventory

    def add_to_inventory(self, items):
        """Updates a player's inventory."""
        for item in items:
            self.inventory.setdefault(item, 0)
            self.inventory[item] += 1

    def display_inventory(self):
        """Takes player's inventory and displays it."""
        total_items = 0
        print('Inventory:')
        for key, value in self.inventory.items():
            total_items += value
            print(f'{value:02d} {key}')
        print(f'Total number of items: {total_items:02d}')


def main():
    """'fantasy_game.py' entry point."""
    # Player 1
    stuff = {
        'rope': 1, 'torch': 6,
        'gold coin': 42, 'dagger': 1,
        'arrow': 12
    }
    dragon_loot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
    inventory = Inventory(stuff)

    # Update player's inventory and displays it afeter that.
    inventory.add_to_inventory(dragon_loot)
    inventory.display_inventory()


if __name__ == '__main__':
    main()
