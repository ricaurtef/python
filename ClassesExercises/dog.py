#!/usr/bin/env python3
"""Object Oriented Programming."""


class Dog:
    """A simple attempt to model a Dog."""
    species = 'Canis familiaris'

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __repr__(self):
        return f'{self.name.capitalize()} is {self.age} years old'

    def speak(self, sound):
        if not isinstance(sound, str):
            raise TypeError('Invalid barking sound.')
        return f'{self.name.capitalize()} barks: {sound}'


class JackRussellTerrier(Dog):
    def speak(self, sound='Arf'):
        return super().speak(sound)


class GoldenRetriever(Dog):
    def speak(self, sound='Bark'):
        return super().speak(sound)


def main():
    """Entry point."""
    miles = Dog('miles', 4)
    miles_speak = miles.speak('Woof Woof')

    budy = JackRussellTerrier('budy', 9)
    budy_speak = budy.speak(1)

    print(miles)
    print(miles_speak)
    print(budy)
    print(budy_speak)


if __name__ == '__main__':
    main()
