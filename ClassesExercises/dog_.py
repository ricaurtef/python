#!/usr/bin/env python3
"""Object Oriented Programming."""


class Dog:
    """A simple attempt to model a dog."""

    def __init__(self, name, age):
        """Initialize name and age attributes."""
        self.name = name
        self.age = age

    def sit(self):
        """Simulate a dog sitting in response to a command."""
        print(f'{self.name.capitalize()} is now sitting.')

    def roll_over(self):
        """Simulate rolling over in response to a command."""
        print(f'{self.name.capitalize()} rolled over!')


def main():
    """Entry point."""
    my_dog = Dog('willie', 6)

    print(f'My dog\'s name is {my_dog.name.title()}.')
    print(f'My dog is {my_dog.age} years old.')
    my_dog.sit()
    my_dog.roll_over()


if __name__ == '__main__':
    main()
