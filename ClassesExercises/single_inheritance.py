#!/usr/bin/env python3
"""Four Pillars of OOP: Inheritance.

Single inheritance example.
"""


class Apple:
    """Parent class Apple Inc."""
    manufacturer = 'Apple Inc.'
    contact_website = 'www.apple.com/contact'

    def contact_details(self):
        """Provides some contact information."""
        print('To contact us, log on to', Apple.contact_website)


class MacBook(Apple):
    """The child class."""
    def __init__(self):
        self.year_of_manufacture = 2017

    def manufacture_details(self):
        """Provides some details about the manufacturer."""
        print(f'This MacBook was manufactured in the year '
              f'{self.year_of_manufacture} by {Apple.manufacturer}')


def main():
    """Entry point."""
    mac_book = MacBook()
    mac_book.manufacture_details()
    mac_book.contact_details()


if __name__ == '__main__':
    main()
