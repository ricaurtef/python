#!/usr/bin/env python3
"""Four Pillars of OOP: Inheritance.

Multiple inheritance example.
"""


class OperatingSystem:
    multitasking = True


class Apple:
    website = 'www.apple.com'


class MacBook(OperatingSystem, Apple):
    def display_details(self):
        if self.multitasking:
            print(f'This is a multitasking system. Visit {Apple.website!r} '
                  'for more details.')


def main():
    """Entry point."""
    mac_book = MacBook()
    mac_book.display_details()


if __name__ == '__main__':
    main()
