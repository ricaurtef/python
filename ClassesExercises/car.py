"""A set of classes used to represent gas and electric cars."""


class Car:
    """A simple attempt to represent a car."""
    def __init__(self, make, model, year):
        """Initialize atributes to describe a car."""
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0

    def __repr__(self):
        """Return a neatly formatted descriptive name."""
        return f'{self.year} {self.make} {self.model}'.title()

    def read_odometer(self):
        """Print a statement showing th car's mileage."""
        print(f'This car has {self.odometer_reading} miles on it.')

    def update_odometer(self, mileage):
        """Set the odoemeter reading to the given value.

        Reject the change if it attempts to roll the odometer back.
        """
        if mileage >= self.odometer_reading:
            self.odometer_reading += mileage
        else:
            print("You can't roll back an odometer!")


class ElectricCar(Car):
    """Represent aspects of a car, specific to electric vehicles."""

    def __init__(self, make, model, year):
        """Initialize attributes of the parent class."""
        super().__init__(make, model, year)
        self.battery = Battery()


class Battery:
    """A simple attempt to model a battery for an electric car.

    An example of 'Instances as Attributes'.
    """

    def __init__(self, battery_size=70):
        """Initialize the battery's attributes."""
        self.battery_size = battery_size

    def __repr__(self):
        """Print a statement describing the battery size."""
        return f'This car has a {self.battery_size}-KWh battery.'

    def get_range(self):
        """Print a statement about the range this battery provides."""
        if self.battery_size == 70:
            range_ = 240
        elif self.battery_size == 85:
            range_ = 270

        print(f'This car go approximately {range_} miles on a full charge.')
