#!/usr/bin/env python3
"""Object Oriented Programming."""
import car


def main():
    """Entry point."""
    my_new_car = car.Car('audi', 'a4', 2016)
    print(my_new_car)
    my_new_car.update_odometer(23500)
    my_new_car.read_odometer()

    # Electric car.
    my_tesla = car.ElectricCar('tesla', 'model s', 2016)
    print(my_tesla)
    print(my_tesla.battery)
    my_tesla.battery.get_range()


if __name__ == '__main__':
    main()
