#!/usr/bin/env python3
"""Classes: Exercises.

Make a class Die with one attribute called sides, which has a default
value of 6. Write a method called roll_die() that prints a random
number between 1 and the number of sides the die has. Make a 6-sided
die and roll it 10 times.
Make a 10-sided die and a 20-sided die. Roll each die 10 times.
"""
from random import randint
from tabulate import tabulate


class Die:
    """A simple attempt to model a die."""

    def __init__(self, side=6):
        """Initialize a 6-sided die by default."""
        self.side = side

    def roll_die(self):
        """Simulate the roll of a die."""
        return randint(1, self.side)


def main():
    """Entry point."""
    six_sided_die = Die()
    ten_sided_die = Die(10)
    twenty_sided_die = Die(20)
    dice = [six_sided_die, ten_sided_die, twenty_sided_die]
    table = {}

    # Fill in the table dict that is printed.
    for die in dice:
        key = f'{die.side}-sided'
        for _ in range(10):
            table.setdefault(key, [])
            table[key] += [die.roll_die()]

    # Pretty printing the data.
    print(tabulate(table, headers='keys', showindex='always', tablefmt='psql'))


if __name__ == '__main__':
    main()
