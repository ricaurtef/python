#!/usr/bin/env python3
"""Challenge:

Find the Factors of a Number.
"""


def main():
    """Entry point."""
    for attempts in range(3):
        try:
            num = int(input('Enter a positive integer: '))
            if num <= 0:
                raise ValueError
        except ValueError:
            continue
        break
    else:
        print('Is it that hard to write a positive number?!')
        exit(1)

    # Find all of the integers that divide evenly a number.
    for i in range(1, num + 1):
        if num % i == 0:
            print(f'{i} is a divisor of {num}')


if __name__ == '__main__':
    main()
