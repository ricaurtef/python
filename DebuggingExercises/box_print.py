#!/usr/bin/env python3
"""Debugging."""


def box_print(symbol, width, height):
    """Generates a char box base on a given symbol."""
    if len(symbol) != 1:
        raise Exception('Symbol must be a single character string.')
    if width <= 2:
        raise Exception('Width must be greater than 2.')
    if height <= 2:
        raise Exception('Height must be greater than 2.')

    print(symbol * width)
    for i in range(height - 2):
        print(symbol + (' ' * (width - 2)) + symbol)
    print(symbol * width)


def main():
    """Script entry point."""
    for sym, w, h in (('*', 4, 4), ('O', 20, 5), ('x', 1, 3), ('ZZ', 3, 3)):
        try:
            box_print(sym, w, h)
        except Exception as err:
            print(f'An exception happened: {err}')


if __name__ == '__main__':
    main()
