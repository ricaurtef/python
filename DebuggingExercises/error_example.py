#!/usr/bin/env python3
"""Debugging."""


def spam():
    bacon()


def bacon():
    raise Exception('This is the error message.')


def main():
    """Script entry point."""
    spam()


if __name__ == '__main__':
    main()
