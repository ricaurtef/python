#!/usr/bin/env python3
'''Funtions:

Exercise: The Collatz Sequence.
'''


def collatz(number):
    '''The simplest impossible math problem.'''
    if number % 2 == 0:
        return number // 2
    return 3 * number + 1


def main():
    '''"collatz.py" entry point.'''
    for _ in range(3):
        try:
            number = int(input('Enter an integer: '))
        except ValueError:
            print('You must enter an integer.')
            continue
        else:
            while number != 1:
                number = collatz(number)
                print(number, end=' ')
            print()
        break
    else:
        print('Is it that hard to enter an integer?')


if __name__ == '__main__':
    main()
