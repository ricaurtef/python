#!/usr/bin/env python3
"""Functions: Zigzag.

A small animation program. This program will create a back-and-forth,
zizag pattern until the user stops it.
"""
from time import sleep


def main():
    """Script entry point."""
    indent = 0  # How many spaces to indent.
    indent_increasing = True    # Wheter the indentation is increasing or not.

    try:
        # The main program loop.
        while True:
            print(f"{' ' * indent}{'*' * 8}")
            sleep(0.1)  # Pause for 1/10 of a second.

            if indent_increasing:
                # Increase the number of spaces.
                indent += 1
                if indent == 20:
                    # Change direction.
                    indent_increasing = False
            else:
                # Decrease the number of spaces.
                indent -= 1
                if not indent:
                    # Change direction.
                    indent_increasing = True
    except KeyboardInterrupt:
        print('\nPretty cool, right?')
        exit()


if __name__ == '__main__':
    main()
