#!/usr/bin/env python3
'''Functions exercise.

Using a function with a while Loop.
'''
from formatted_name import get_formatted_name


def main():
    '''"greeter.py" entry point.'''
    while True:
        print('\nPlease tell me your name: (enter "q" to quit)')
        first_name = input('First name: ').lower()
        if first_name in ('q', 'quit'):
            break

        last_name = input('Last name: ').lower()
        if last_name in ('q', 'quit'):
            break

        formatted_name = get_formatted_name(first_name, last_name)
        print(f'\nHello, {formatted_name}!')


if __name__ == '__main__':
    main()
