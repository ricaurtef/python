#!/usr/bin/env python3
"""Functions: Example."""
from random import choice


def main():
    """Script entry point."""
    answers = [
        'It is certain',
        'It is decidedly so',
        'Yes',
        'Reply hazy try again',
        'Ask again later',
        'Concentrate and ask again',
        'My reply is no',
        'Outlook not so good',
        'Very doubtful',
    ]
    print(choice(answers))


if __name__ == '__main__':
    main()
