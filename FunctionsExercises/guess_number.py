#!/usr/bin/env python3
'''Functions:

This is a guess the number game.
'''
import random


def error_message():
    """Gives a random error message."""
    messages = [
        'Wrong!',
        'Please enter a number.',
        'Dude! Just give me a number.'
    ]
    return random.choice(messages)


def main():
    '''"guess_number.py" entry point.'''
    secret_number = random.randint(1, 20)
    print('I am thinking of a number between 1 and 20.')

    # Ask the player to guess 6 times.
    for attempts in range(6):
        try:
            guess = int(input('Take a guess: '))
        except ValueError:
            print(error_message())
            continue

        if guess < secret_number:
            print('Your guess is too low.')
        elif guess > secret_number:
            print('Your guess is too high.')
        else:
            print(f'Good job! You guessed my number in {attempts + 1} '
                  'attempts!')
            break
    else:
        print(f'Nope. The number I was thinking of was "{secret_number}".')


if __name__ == '__main__':
    main()
