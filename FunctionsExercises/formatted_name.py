#!/usr/bin/env python3
'''Python Functions Exercise.

Returning a simple vale.
'''


def get_formatted_name(first_name, last_name, middle_name=''):
    '''Return a full name, neatly formatted.'''
    if middle_name:
        full_name = f'{first_name} {middle_name} {last_name}'
    else:
        full_name = f'{first_name} {last_name}'
    return full_name.title()


def main():
    '''"formatted_name.py" entry point.'''
    musicians = [
        {'name': 'jimi', 'last_name': 'hendrix'},
        {'name': 'john', 'middle_name': 'hooker', 'last_name': 'lee'}
    ]

    for musician in musicians:
        if 'middle_name' in musician:
            full_name = get_formatted_name(
                musician['name'],
                musician['last_name'],
                musician['middle_name']
            )
        else:
            full_name = get_formatted_name(
                musician['name'],
                musician['last_name']
            )
        print(full_name)


if __name__ == '__main__':
    main()
