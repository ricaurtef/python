#!/usr/bin/env python3
"""Challenge:

Turn your user into a L33t H4xor.
"""


def main():
    """Script entry point."""
    text = input('Enter some text: ')
    text = text.replace('a', '4')
    text = text.replace('b', '8')
    text = text.replace('e', '3')
    text = text.replace('l', '1')
    text = text.replace('o', '0')
    text = text.replace('s', '5')
    text = text.replace('t', '7')

    print(text)


if __name__ == '__main__':
    main()
